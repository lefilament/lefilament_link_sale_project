# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from . import product
from . import sale_config_settings
from . import res_config
from . import project_task_type
from . import project
from . import project_task
from . import sale_order